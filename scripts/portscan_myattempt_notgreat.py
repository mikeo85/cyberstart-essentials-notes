# MY ATTEMPT
import socket

client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

ipAddr = '127.0.0.1'
openPorts = []
closedPorts = []

for port in range(0,65536):
    response = client_socket.connect_ex((ipAddr, port))
    if response == 0:
        openPorts.append(port)
    else:
        closedPorts.append(port)
    port+=1
print("openPorts", openPorts)
