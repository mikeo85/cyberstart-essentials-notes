import socket

server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
server_socket.bind(("0.0.0.0", 1337))
# use bind() to take ownership of the IP address and port number
# in the tuple if not being used by any other program. Use of 0.0.0.0
# tells the program to listen on every IP address assigned to the
# computer it is running on.
server_socket.listen(10) # make server listen on bound port

while True: # intentionally creating an infinite loop
    # server will never quit unless manually interrupted (Ctl+c)
    conn, addr = server_socket.accept() # establish connection with client
    conn.send("Do you want to play a game?\n")
    received = conn.recv(1024)
    print(received)

server_socket.close()

# To use this script, run two terminals.
# First run this script to create TCP server.
# Then make a connection using
# 'nc 127.0.0.1 1337'
# Use Ctl+c on the terminal running this script to close.
