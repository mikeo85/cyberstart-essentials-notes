import socket
import thread

# Thread handler
def handler(client_sock, address):
    client_sock.send("Do you want to play a game?\n")
    data = client_sock.recv(1024)
    print(repr(address) + " said: " + data)
    client_sock.close()
    print(repr(address) + " connection ended.")

# Set up our server
server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
server_socket.bind(("0.0.0.0", 1337))
server_socket.listen(10)

# Run the server with threads
while True:
    print("Server listening for connections...")

    client_sock, address = server_socket.accept()
    print("Connection from: " + repr(address))

    thread.start_new_thread(handler, (client_sock, address))

# To run:
# Run server in one terminal
# In multiple other terminals (to demonstrate threading),
# run 'nc 127.0.0.1 1337'
# If you just run the 'nc' command and dont provide any input, then the
# thread remains open, demonstrating multiple threads connected to the
# same program
