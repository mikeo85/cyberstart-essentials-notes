import socket

server_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
server_socket.bind(("0.0.0.0", 1337))

while True:
    data, addr = server_socket.recvfrom(1024)
    print(str(addr) + " -- " + str(data))

# To run:
# Run 'python udpserver.py' to start server
# In second terminal, open connection with 'nc -u 127.0.0.1 1337'
# Enter some text at the prompt and hit enter
# Text per print() command above will print to the python terminal
