# Cyberstart Essentials 53-4.
# Creating a simple TCP connection using Python and the 'socket' library

import socket # import socket library,
              # which is part of python's standard library

client_socket = socket.socket( # create a socket object
    socket.AF_INET, # pass argument to use IPv4
    socket.SOCK_STREAM # pass argument to use TCP
)
client_socket.connect(('127.0.0.1', 1337)) #initiate connection to socket,
    # creating tuple which contains IP address and port number
    # to create connection
client_socket.send("Do you want to play a game?\n") # send data over connection
received = client_socket.recv(1024) # stores returned value in variable,
    # limiting maximum bytes to 1024
print(received) # print content
client_socket.close() # close socket (initiating TCP teardown prcocess)

# To run this, open two terminals. In one, open a server listening on
# TCP port 1337 by using 'nc -l 1337'
# In the other, run this script using 'python tcpclient.py'
