import socket

client_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
client_socket.sendto("UDP is connectionless...\n", ("127.0.0.1", 1337))

# To run:
# Use netcat to set up server with 'nc -u -l 1337'
# In second terminal, run 'python udpclient.py
# Once py script is run, the nc terminal will display the text above
# then the python script will have completed in the other terminal
