# MY ATTEMPT
# import socket

# client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

# ipAddr = '127.0.0.1'
# openPorts = []
# closedPorts = []

# for port in range(0,65536):
#     response = client_socket.connect_ex((ipAddr, port))
#     if response == 0:
#         openPorts.append(port)
#     else:
#         closedPorts.append(port)
#     port+=1
# print("openPorts", openPorts)

# COURSE RECOMMENDATION

import socket

print("Please enter an IP Address to scan.")
target = raw_input("> ")

print("*" * 40)
print("* Scanning: " + target + " *")
print("*" * 40)

for port in range(1, 65536):
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    result = s.connect_ex((target, port))
    if result == 0:
        print("Port: " + str(port) + " Open")
    s.close()
